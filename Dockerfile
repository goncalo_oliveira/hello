FROM alpine:3.4

RUN apk --no-cache add nginx

ADD www /www
ADD nginx.conf /etc/nginx/

RUN mkdir /etc/nginx/logs

EXPOSE 80

CMD ["nginx", "-g", "pid /tmp/nginx.pid; daemon off;"]
